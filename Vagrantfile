# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  if Vagrant.has_plugin?("vagrant-proxyconf")
    config.proxy.http     = "http://10.0.2.2:3128"
    config.proxy.https    = "http://10.0.2.2:3128"
    config.proxy.no_proxy = "localhost,127.0.0.1,.example.com,192.169.33.0/24"
    config.apt_proxy.http  = "http://10.0.2.2:3128"
    config.apt_proxy.https = "http://10.0.2.2:3128"
  end

  config.vm.define "head" do |head|
    head.vm.box       = "bento/ubuntu-16.04"
    head.vm.host_name = "head"

    head.vm.network "private_network" ,ip: "192.169.33.20"
    head.vm.provision :hosts, :sync_hosts => true
    head.ssh.forward_agent = true

    config.vm.provision "k8s-head" , type: :shell, path: "install-k8s-head.sh"

    head.vm.provider :virtualbox do |vb|
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.customize ["modifyvm", :id, "--memory", "2048"]
      vb.customize ["modifyvm", :id, "--cpus", "2"]
      vb.customize ["modifyvm", :id, "--name", "head"]
    end
  end

  config.vm.define "worker-one" do |worker|
    worker.vm.box       = "bento/ubuntu-16.04"
    worker.vm.host_name = "worker-one"

    worker.vm.network "private_network" ,ip: "192.169.33.25"
    worker.vm.provision :hosts, :sync_hosts => true
    worker.ssh.forward_agent = true

    config.vm.provision "k8s-worker" , type: :shell, path: "install-k8s-worker.sh"

    worker.vm.provider :virtualbox do |vb|
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.customize ["modifyvm", :id, "--memory", "1024"]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
      vb.customize ["modifyvm", :id, "--name", "worker-one"]
    end
  end

  config.vm.define "worker-two" do |worker|
    worker.vm.box       = "bento/ubuntu-16.04"
    worker.vm.host_name = "worker-two"

    worker.vm.network "private_network" ,ip: "192.169.33.30"
    worker.vm.provision :hosts, :sync_hosts => true
    worker.ssh.forward_agent = true

    config.vm.provision "k8s-worker" , type: :shell, path: "install-k8s-worker.sh"

    worker.vm.provider :virtualbox do |vb|
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.customize ["modifyvm", :id, "--memory", "1024"]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
      vb.customize ["modifyvm", :id, "--name", "worker-two"]
    end
  end

end
